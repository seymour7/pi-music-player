#!/usr/bin/env bash

if [ `whoami` != "root" ]; then
  echo "Please run this as root!"
  exit 1
fi

# Automatic mounting of devices is achieved with udisks2
pacman -Sy --noconfirm --needed udiskie

# Allow all members of the storage group to run udiskie
cat > /etc/polkit-1/rules.d/50-udiskie.rules <<EOF
polkit.addRule(function(action, subject) {
  var YES = polkit.Result.YES;
  // NOTE: there must be a comma at the end of each line except for the last:
  var permission = {
    // required for udisks1:
    "org.freedesktop.udisks.filesystem-mount": YES,
    "org.freedesktop.udisks.luks-unlock": YES,
    "org.freedesktop.udisks.drive-eject": YES,
    "org.freedesktop.udisks.drive-detach": YES,
    // required for udisks2:
    "org.freedesktop.udisks2.filesystem-mount": YES,
    "org.freedesktop.udisks2.encrypted-unlock": YES,
    "org.freedesktop.udisks2.eject-media": YES,
    "org.freedesktop.udisks2.power-off-drive": YES,
    // required for udisks2 if using udiskie from another seat (e.g. systemd):
    "org.freedesktop.udisks2.filesystem-mount-other-seat": YES,
    "org.freedesktop.udisks2.filesystem-unmount-others": YES,
    "org.freedesktop.udisks2.encrypted-unlock-other-seat": YES,
    "org.freedesktop.udisks2.eject-media-other-seat": YES,
    "org.freedesktop.udisks2.power-off-drive-other-seat": YES
  };
  if (subject.isInGroup("storage")) {
    return permission[action.id];
  }
});
EOF

# Add user 'alarm' to the group 'storage'
gpasswd -a alarm storage

# Create a systemd unit file that runs udiskie
cat > /etc/systemd/system/udiskie.service <<EOF
[Unit]
Description=Udiskie

[Service]
ExecStart=/usr/bin/udiskie
User=alarm

[Install]
WantedBy=multi-user.target 
EOF

# Enable the udiskie unit so it starts at boot time
systemctl enable udiskie.service

# Install remaining required packages
pacman -Sy --noconfirm --needed python2 flite

#packer -Sy --noconfirm omxplayer-git # arch linux arm might come with this?

# Retrieve source code for the music player
if [ ! -d "/home/alarm/pi-music-player" ]; then
  git clone https://seymour7@bitbucket.org/seymour7/pi-music-player.git /home/alarm/pi-music-player
fi

# Create a systemd unit file that runs the music player 
cat > /etc/systemd/system/pi-music-player.service <<EOF
[Unit]
Description=Pi Music Player

[Service]
WorkingDirectory=/home/alarm/pi-music-player
ExecStart=/home/alarm/pi-music-player/player.py
User=alarm

[Install]
WantedBy=multi-user.target 
EOF

# Enable the unit so the music player starts at boot time
systemctl enable pi-music-player.service