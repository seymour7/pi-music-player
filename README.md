# Pi Music Player

Converts a Raspberry Pi into a music player. Once booted, the Pi will start playing music from the first detected USB drive (outputting to the 3.5mm audio jack).

## Features
* Supports many audio encodings ([omxplayer](https://elinux.org/Omxplayer))
* Supports playlists via directories on the USB drive
* Continues playing the previous playlist on boot
* Supports "next song" and "next playlist" button
* Uses [udiskie](https://github.com/coldfix/udiskie) to continuously mount any USB drive that is plugged in (hence, you can switch USB drives while music is playing)

## Installation

To get this running on your Raspberry Pi, follow:
1. Install [Arch Linux ARM](https://archlinuxarm.org) on your Pi's SD card
2. Login to your Pi as root
3. `curl -s 'https://raw.githubusercontent.com/seymour7/pi-music-player/master/install.sh' | sudo sh`

## Connecting buttons to Pi

Attach push buttons to pin 17 and 27 without and pull up/down resistors (Raspberry Pi has built-in pull-up and pull-down resistors which are be enabled in player.py).
http://raspi.tv/wp-content/uploads/2013/07/RasPi.TV-RPi.GPIO-input-output2_bb-1024x781.png

## Structure for files on usb drive

/
  Playlist
     files
  Playlist 2
     files

## Usage

Once everything is installed, just boot the Pi, plug in a USB drive and connect some speakers/headphones to the audio jack.

## Troubleshooting

### How do I get a channel's id?

Run:

```
curl https://www.googleapis.com/youtube/v3/channels?part=id&forUsername=PewDiePie&key={YOUR_API_KEY}
```

Or, visit the channel's url (i.e. https://www.youtube.com/user/PewDiePie), right click, view page source and search for 'channelId'.

### Downloaded video will not play

Sometimes, if Streamlink ends abruptly, vlc will have problems decoding the video file. You can use `ffmpeg` to fix the encoding:

```
ffmpeg -i input.mp4 -c copy fixed.mp4
```

## Contributing

Contributions are welcome! Please feel free to submit a Pull Request.